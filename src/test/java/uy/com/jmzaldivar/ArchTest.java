package uy.com.jmzaldivar;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("uy.com.jmzaldivar");

        noClasses()
            .that()
                .resideInAnyPackage("uy.com.jmzaldivar.service..")
            .or()
                .resideInAnyPackage("uy.com.jmzaldivar.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..uy.com.jmzaldivar.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
