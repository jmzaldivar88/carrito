package uy.com.jmzaldivar.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uy.com.jmzaldivar.domain.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
}
