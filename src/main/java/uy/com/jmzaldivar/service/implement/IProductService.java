package uy.com.jmzaldivar.service.implement;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.service.dto.ProductDTO;
import uy.com.jmzaldivar.web.rest.dto.Result;

import java.util.List;

@Service
public interface IProductService {

    Result<ProductDTO> create(ProductDTO product) ;
    Result<ProductDTO> modify(ProductDTO product) ;
    Result<ProductDTO> get(String uuid);
    Result<Boolean> delete(String uuid) ;
    Result<List<ProductDTO>> listAll();
    Result<ProductDTO> productById(String uuid) ;
}
