package uy.com.jmzaldivar.service.implement;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.service.dto.TotalDto;
import uy.com.jmzaldivar.service.dto.ProductCartDTO;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.service.dto.ShoppingCartDTO;

import java.util.List;
@Service
public interface IShoppingCartService {

    Result<Boolean> create(ShoppingCartDTO cartDTO);
    Result<Boolean> modify(ShoppingCartDTO cartDTO);
    Result<Boolean> delete(String uuidProduct);
    Result<List<ProductCartDTO>> listProductByCarrito(Boolean checkout);
    Result<TotalDto> checkout();
}
