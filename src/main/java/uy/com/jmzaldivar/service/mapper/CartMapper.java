package uy.com.jmzaldivar.service.mapper;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.ShoppingCart;
import uy.com.jmzaldivar.service.enumerator.StateCart;
import uy.com.jmzaldivar.service.dto.CartDTO;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.service.transactional.ProductTransactional;
import uy.com.jmzaldivar.web.rest.utils.Variables;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CartMapper implements EntityMapper<CartDTO, ShoppingCart> {


    private final ProductTransactional productTransactional;
    private final ProductMapper productMapper;

    public CartMapper(ProductTransactional productTransactional,ProductMapper productMapper) {
        this.productTransactional = productTransactional;
        this.productMapper = productMapper;
    }

    @Override
    public ShoppingCart toEntity(CartDTO dto) throws BusinessException {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUsuario(dto.getUsername());
        shoppingCart.setCantidad(dto.getAmount());
        shoppingCart.setProduct(this.productTransactional.get(dto.getProducto().getIdProduct()).orElseThrow(()->new BusinessException(String.format(Variables.MSG_NOT_EXIST_ENTITY,Variables.ENTITY_NAME_PRODUCT,dto.getProducto().getIdProduct()))));
        StateCart stateCart = StateCart.PENDIENTE;
        if(StateCart.COMPLETADO.getValue().equals(dto.getState()))
            stateCart = StateCart.COMPLETADO;

        shoppingCart.setStateCart(stateCart);


        return shoppingCart;
    }

    @Override
    public CartDTO toDto(ShoppingCart entity) {
        CartDTO cartDTO = new CartDTO();
        cartDTO.setUsername(entity.getUsuario());
        cartDTO.setState(entity.getStateCart().getValue());
        cartDTO.setAmount(entity.getCantidad());
        cartDTO.setProducto(this.productMapper.toDto(entity.getProduct()));
        return cartDTO;
    }

    @Override
    public List<ShoppingCart> toEntity(List<CartDTO> dtoList) throws BusinessException{
       return dtoList.stream().map(cartDTO -> {
           try {
               return this.toEntity(cartDTO);
           } catch (BusinessException e) {
               e.printStackTrace();
           }
           return null;
       }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    public List<CartDTO> toDto(List<ShoppingCart> entityList) {
        return entityList.stream().map(shoppingCart -> this.toDto(shoppingCart)).collect(Collectors.toList());
    }
}
