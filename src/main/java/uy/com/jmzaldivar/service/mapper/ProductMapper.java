package uy.com.jmzaldivar.service.mapper;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.service.dto.ProductDTO;
import uy.com.jmzaldivar.service.dto.ProductoDescuentoDTO;
import uy.com.jmzaldivar.service.enumerator.TypeProduct;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductMapper implements EntityMapper<ProductDTO,Product> {


    @Override
    public Product toEntity(ProductDTO dto) {
        Product product = new Product();
        product.setIdProduct(dto.getIdProduct());
        product.setSku(dto.getSku());
        product.setDescription(dto.getDescription());
        product.setName(dto.getName());
        product.setTypeProduct(dto.getTypeProduct());
        product.setPrice(dto.getPrice());
        if(dto instanceof ProductoDescuentoDTO)
            product.setDiscount(new BigDecimal("0.5"));


        return product;
    }

    @Override
    public ProductDTO toDto(Product entity) {
        ProductDTO productDTO = new ProductDTO(); ;

        if(TypeProduct.DESCUENTO.equals(entity.getTypeProduct())) {
            productDTO = new ProductoDescuentoDTO();
            ((ProductoDescuentoDTO)productDTO).setDiscount(entity.getDiscount());
        }

        productDTO
            .idProduct(entity.getIdProduct())
            .price(entity.getPrice())
            .sku(entity.getSku())
            .description(entity.getDescription())
            .name(entity.getName())
            .typeProduct(entity.getTypeProduct());

        return productDTO;
    }

    @Override
    public List<Product> toEntity(List<ProductDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ProductDTO> toDto(List<Product> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}
