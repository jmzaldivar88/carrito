package uy.com.jmzaldivar.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartDTO {

    @JsonIgnoreProperties
    @JsonProperty(value = "username")
    private String username = "admin";

    @JsonProperty(value = "amount")
    private Integer amount;

    @JsonIgnoreProperties
    @JsonProperty(value = "state")
    private String state;

    @JsonProperty(value = "producto")
    private ProductDTO producto;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ProductDTO getProducto() {
        return producto;
    }

    public void setProducto(ProductDTO producto) {
        this.producto = producto;
    }

}
