package uy.com.jmzaldivar.service.dto;


import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.DiscriminatorOptions;
import uy.com.jmzaldivar.domain.implement.IProduct;
import uy.com.jmzaldivar.service.enumerator.TypeProduct;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import java.math.BigDecimal;

@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "typeProduct")
@DiscriminatorValue(value = "SIMPLE")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "typeProduct", visible = true)
@JsonSubTypes({
    @JsonSubTypes.Type(value = ProductoDescuentoDTO.class,name = "DESCUENTO"),
    @JsonSubTypes.Type(value = ProductDTO.class,name = "SIMPLE")
})
@DiscriminatorOptions(force = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

public class ProductDTO implements IProduct {

    @JsonProperty(value = "idProduct")
    private  String idProduct;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "sku")
    private String sku;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "price")
    private BigDecimal price;

    @JsonProperty(value = "typeProduct")
    private TypeProduct typeProduct;


    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public ProductDTO idProduct(String idProduct) {
        this.idProduct = idProduct;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductDTO name(String name) {
        this.name = name;
        return this;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public ProductDTO sku(String sku) {
        this.sku = sku;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductDTO description(String description) {
        this.description = description;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ProductDTO price(BigDecimal price) {
        this.price = price;
        return this;
    }


    public TypeProduct getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(TypeProduct typeProduct) {
        this.typeProduct = typeProduct;
    }

    public ProductDTO typeProduct(TypeProduct typeProduct) {
        this.typeProduct = typeProduct;
        return this;
    }



    @Override
    public BigDecimal priceFinal() {
        return this.getPrice();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("'idProduct':").append(idProduct).append(",\n");
        sb.append("'name':").append(name).append(",\n");
        sb.append("'description':").append(description).append(",\n");
        sb.append("'sku':").append(sku).append(",\n");
        sb.append("'typeProduct':").append(typeProduct).append(",\n");
        sb.append("'price':").append(price).append(",\n");
        sb.append("'price_final':").append(this.priceFinal()).append("\n");
        sb.append("}");
        return sb.toString() ;
    }
}
