package uy.com.jmzaldivar.service.dto;

import uy.com.jmzaldivar.domain.implement.IShoppingCart;

import java.math.BigDecimal;

public class ProductCartDTO implements IShoppingCart {

    private Integer amount;
    private ProductDTO productDTO;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
    public ProductCartDTO amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public ProductDTO getProductDTO() {
        return productDTO;
    }

    public void setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }
    public ProductCartDTO productDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
        return this;
    }

    @Override
    public BigDecimal total() {
        return productDTO.priceFinal().multiply(new BigDecimal(this.amount));
    }
}
