package uy.com.jmzaldivar.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShoppingCartDTO {

    @ApiModelProperty(required = true, value = "")
    @JsonProperty(value = "amount")
    private Integer amount;

    @ApiModelProperty(required = true, value = "")
    @JsonProperty(value = "product_id")
    private String idProducto;



    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }
}
