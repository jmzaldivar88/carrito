package uy.com.jmzaldivar.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"discount"})
public class ProductoDescuentoDTO extends ProductDTO{

    @JsonIgnoreProperties
    @JsonProperty(value = "discount")
    private BigDecimal discount = new BigDecimal("0.5");



    @JsonIgnore
    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Override
    public BigDecimal priceFinal() {
        return super.getPrice().subtract(super.getPrice().multiply(this.getDiscount()));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("'idProduct':").append(super.getIdProduct()).append(",\n");
        sb.append("'name':").append(super.getName()).append(",\n");
        sb.append("'description':").append(super.getDescription()).append(",\n");
        sb.append("'sku':").append(super.getSku()).append(",\n");
        sb.append("'typeProduct':").append(super.getTypeProduct()).append(",\n");
        sb.append("'price':").append(super.getPrice()).append(",\n");
        sb.append("'discount':").append(discount).append(",\n");
        sb.append("'price_final':").append(this.priceFinal()).append("\n");
        sb.append("}");
        return sb.toString() ;
    }

}
