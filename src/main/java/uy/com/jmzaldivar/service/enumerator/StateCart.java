package uy.com.jmzaldivar.service.enumerator;

public enum StateCart {
    PENDIENTE("PENDIENTE"),
    COMPLETADO("COMPLETADO");

    String value;

    StateCart(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
