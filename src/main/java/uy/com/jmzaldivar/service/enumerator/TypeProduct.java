package uy.com.jmzaldivar.service.enumerator;


public enum TypeProduct {
    SIMPLE("SIMPLE"),
    DESCUENTO("DESCUENTO");
    String value;

    TypeProduct(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
