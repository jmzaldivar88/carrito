package uy.com.jmzaldivar.service.transactional;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.domain.ShoppingCart;

import java.util.List;
import java.util.Optional;

@Service
public interface IShoppingCartTransactional {

    ShoppingCart create(ShoppingCart shoppingCart);
    List<ShoppingCart>  createAll(List<ShoppingCart> shoppingCartList);
    ShoppingCart modify(ShoppingCart shoppingCart);
    Optional<ShoppingCart> get(String username, Product product);
    void delete(String username);
    void deleteProductCart(String username, Product product);
    List<ShoppingCart> listProductos(String username);
}
