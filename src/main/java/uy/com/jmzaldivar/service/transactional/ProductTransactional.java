package uy.com.jmzaldivar.service.transactional;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductTransactional implements IProductTransactional {
    private final ProductRepository productRepository;

    public ProductTransactional(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product create(Product product) {
        return this.productRepository.save(product);
    }

    @Override
    public Product modify(Product product) {

        return this.productRepository.save(product);
    }

    @Override
    public Optional<Product> get(String uuid) {
        return this.productRepository.findById(uuid);
    }

    @Override
    public List<Product> get(List<String> uuids) {
        return this.productRepository.findAllById(uuids);
    }

    @Override
    public void delete(Product product) {
        this.productRepository.delete(product);
    }

    @Override
    public List<Product> getAll() {
        return this.productRepository.findAll();
    }


}
