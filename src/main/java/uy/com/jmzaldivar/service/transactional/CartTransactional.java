package uy.com.jmzaldivar.service.transactional;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.domain.ShoppingCart;
import uy.com.jmzaldivar.service.enumerator.StateCart;
import uy.com.jmzaldivar.repository.ShoppingCartRepository;

import java.util.*;

@Service
public class CartTransactional implements IShoppingCartTransactional {
    private final ShoppingCartRepository shoppingCartRepository;

    public CartTransactional(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @Override
    public ShoppingCart create(ShoppingCart shoppingCart) {
        return this.shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public List<ShoppingCart> createAll(List<ShoppingCart> shoppingCartList) {
        return this.shoppingCartRepository.saveAll(shoppingCartList);
    }

    @Override
    public ShoppingCart modify(ShoppingCart shoppingCart) {
        return this.shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public Optional<ShoppingCart> get(String username, Product product) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUsuario(username);
        shoppingCart.setProduct(product);
        Example<ShoppingCart> example = Example.of(shoppingCart);
        return this.shoppingCartRepository.findOne(example);
    }

    @Override
    public void delete(String username) {
        this.shoppingCartRepository.deleteAll(this.listProductos(username));
    }

    @Override
    public void deleteProductCart(String username, Product product) {
        List<ShoppingCart>  list = this.listProductos(username);
        ShoppingCart shoppingCart = list.stream().filter(shoppingCart1 -> product.equals(shoppingCart1.getProduct())).findFirst().orElse(null);
        if(shoppingCart != null)
            this.shoppingCartRepository.delete(shoppingCart);
    }


    @Override
    public List<ShoppingCart> listProductos(String username) {

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUsuario(username);
        shoppingCart.setStateCart(StateCart.PENDIENTE);
        Example<ShoppingCart> example = Example.of(shoppingCart);

        return this.shoppingCartRepository.findAll(example);
    }

}
