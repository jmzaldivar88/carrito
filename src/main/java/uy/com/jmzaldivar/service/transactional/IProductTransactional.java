package uy.com.jmzaldivar.service.transactional;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;

import java.util.List;
import java.util.Optional;

@Service
public interface IProductTransactional {
    Product create(Product product);
    Product modify(Product product);
    Optional<Product> get(String uuid);
    List<Product> get(List<String> uuid);
    void delete(Product product);
    List<Product> getAll();
}
