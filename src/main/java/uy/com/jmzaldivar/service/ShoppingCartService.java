package uy.com.jmzaldivar.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.domain.ShoppingCart;
import uy.com.jmzaldivar.security.SecurityUtils;
import uy.com.jmzaldivar.service.dto.CartDTO;
import uy.com.jmzaldivar.service.dto.ProductDTO;
import uy.com.jmzaldivar.service.dto.TotalDto;
import uy.com.jmzaldivar.service.enumerator.StateCart;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.service.implement.IShoppingCartService;
import uy.com.jmzaldivar.service.mapper.CartMapper;
import uy.com.jmzaldivar.service.mapper.ProductMapper;
import uy.com.jmzaldivar.service.transactional.CartTransactional;
import uy.com.jmzaldivar.service.transactional.ProductTransactional;
import uy.com.jmzaldivar.service.dto.ProductCartDTO;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.service.dto.ShoppingCartDTO;
import uy.com.jmzaldivar.web.rest.utils.Variables;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class ShoppingCartService implements IShoppingCartService {

    private final CartTransactional cartTransactional;
    private final ProductTransactional productTransactional;
    private final CartMapper cartMapper;
    private final ProductMapper productMapper;


    public ShoppingCartService(CartTransactional cartTransactional,ProductTransactional productTransactional, CartMapper cartMapper,ProductMapper productMapper) {
        this.cartTransactional = cartTransactional;
        this.productTransactional = productTransactional;
        this.cartMapper = cartMapper;
        this.productMapper =productMapper;
    }

    @Override
    public Result<Boolean> create(ShoppingCartDTO cartDTO) {
        Result<Boolean> result = new Result<>();
        try {
            String username = SecurityUtils.getCurrentUserJWT().orElse("admin");
            CartDTO create = new CartDTO();
            create.setState(StateCart.PENDIENTE.getValue());
            create.setProducto(new ProductDTO().idProduct(cartDTO.getIdProducto()));
            create.setAmount(cartDTO.getAmount());
            create.setUsername(username);
            this.cartTransactional.create(this.cartMapper.toEntity(create));
            result.setData(Boolean.TRUE);
            result.setStatus(HttpStatus.CREATED.value());
        }catch (Exception exception){
            result.setError(exception.getMessage());
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setData(Boolean.FALSE);
        }
        return result;
    }

    @Override
    public Result<Boolean> modify(ShoppingCartDTO cartDTO) {
        Result<Boolean> result = new Result<>();
        try {
            String username = SecurityUtils.getCurrentUserJWT().orElse("admin");
            Optional<ShoppingCart> opCart = this.cartTransactional.get(username,this.productTransactional.get(cartDTO.getIdProducto()).orElseThrow(()->new BusinessException(String.format(Variables.MSG_NOT_EXIST_ENTITY,Variables.ENTITY_NAME_SHOPPING_CART,cartDTO.getIdProducto()))));
            opCart.ifPresent(this.cartTransactional::modify);
            result.setData(Boolean.TRUE);
            result.setStatus(HttpStatus.ACCEPTED.value());
        }catch (Exception exception){
            result.setError(exception.getMessage());
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setData(Boolean.FALSE);
        }
        return result;
    }

    @Override
    public Result<Boolean> delete(String uuidProduct) {
        Result<Boolean> result = new Result<>();
        try {
            String username = SecurityUtils.getCurrentUserJWT().orElse("admin");
            this.cartTransactional.deleteProductCart(username,this.productTransactional.get(uuidProduct).orElse(new Product()));
            result.setStatus(HttpStatus.OK.value());
            result.setData(Boolean.TRUE);
        }catch (Exception exception){
            result.setError(exception.getMessage());
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setData(Boolean.FALSE);
        }
        return result;
    }

    @Override
    public Result<List<ProductCartDTO>> listProductByCarrito(Boolean checkout) {
        Result<List<ProductCartDTO>>  result = new Result<>();
        try {
            String username = SecurityUtils.getCurrentUserJWT().orElse("admin");
            List<ProductCartDTO> list = this.cartTransactional.listProductos(username).stream().map(shoppingCart -> {
                if(checkout){
                    shoppingCart.setStateCart(StateCart.COMPLETADO);
                    this.cartTransactional.modify(shoppingCart);
                }
                return shoppingCart;
            }).map(shoppingCart -> new ProductCartDTO().amount(shoppingCart.getCantidad()).productDTO(this.productMapper.toDto(shoppingCart.getProduct()))).collect(Collectors.toList());
                list.forEach(productDTO -> {
                    if (!checkout)
                        productDTO.getProductDTO().setPrice(productDTO.getProductDTO().priceFinal());
            });
            result.setStatus(HttpStatus.OK.value());
            result.setData(list);
        }catch (Exception exception){
            result.setError(exception.getMessage());
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setData(Collections.emptyList());
        }
        return result;
    }

    @Override
    public Result<TotalDto> checkout() {
        Result<TotalDto> result = new Result<>();
        result.setStatus(HttpStatus.OK.value());
        try {
            Result<List<ProductCartDTO>> listcarrito = this.listProductByCarrito(Boolean.TRUE);

            if(listcarrito.getStatus() == HttpStatus.OK.value()){
                List<ProductCartDTO> productCartDTOS = listcarrito.getData();
                AtomicReference<BigDecimal> sum = new AtomicReference<>(BigDecimal.ZERO);
                productCartDTOS.forEach(cartDTO -> {
                    sum.set(sum.get().add(cartDTO.total()));
                });
                TotalDto totalDto = new TotalDto();
                totalDto.setTotal(sum.get());
                result.setData(totalDto);
            }else{
                result.setStatus(listcarrito.getStatus());
                result.setError(listcarrito.getError());
            }
        } catch (Exception exception){
            result.setError(exception.getMessage());
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
       return  result;
    }
}
