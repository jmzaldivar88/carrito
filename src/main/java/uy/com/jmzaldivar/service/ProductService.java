package uy.com.jmzaldivar.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.service.dto.ProductDTO;
import uy.com.jmzaldivar.service.dto.ProductoDescuentoDTO;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.service.implement.IProductService;
import uy.com.jmzaldivar.service.mapper.ProductMapper;
import uy.com.jmzaldivar.service.transactional.IProductTransactional;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.web.rest.errors.BadRequestAlertException;
import uy.com.jmzaldivar.web.rest.utils.Variables;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

@Service
public class ProductService implements IProductService {
    private final IProductTransactional iProductTransactional;
    private final ProductMapper productMapper;

    public ProductService(IProductTransactional iProductTransactional,ProductMapper productMapper) {
        this.iProductTransactional = iProductTransactional;
        this.productMapper = productMapper;
    }

    @Override
    public Result<ProductDTO>  create(ProductDTO productDto) {
        Result<ProductDTO> result = new Result<>();
        try {
            Product product1 = this.productMapper.toEntity(productDto);

            result.setData(this.productMapper.toDto(this.iProductTransactional.create(product1)));
            result.setStatus(HttpStatus.CREATED.value());
        }catch (Exception exception){
            result.setError(exception.getMessage());
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return result;
    }

    @Override
    public Result<ProductDTO> modify(ProductDTO product)  {

        Result<ProductDTO> result = new Result<>();
        result.setStatus(HttpStatus.ACCEPTED.value());


        Product update = this.iProductTransactional.get(product.getIdProduct()).orElse(null);


        if(update == null){
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setError(String.format(Variables.MSG_NOT_EXIST_ENTITY,Variables.ENTITY_NAME_PRODUCT,product.getIdProduct()));
            return result;
        }

            update.setDescription((StringUtils.isNotBlank(product.getDescription()))?product.getDescription(): update.getDescription());
            update.setName((StringUtils.isNotBlank(product.getName()))?product.getName(): update.getName());
            update.setSku((StringUtils.isNotBlank(product.getSku()))?product.getSku(): update.getSku());
            update.setTypeProduct(product.getTypeProduct() != null ?product.getTypeProduct(): update.getTypeProduct());
            update.setPrice(product.getPrice() != null ?product.getPrice(): update.getPrice());
            update.setDiscount(BigDecimal.ZERO);

            if(product instanceof ProductoDescuentoDTO){
                update.setDiscount(new BigDecimal("0.5"));
            }

            Logger.getLogger(this.getClass().getName()).info(update.getPrice().toString());
            try {
                result.setData(this.productMapper.toDto(this.iProductTransactional.modify(update)));
            }catch (Exception exception){
                result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                result.setError(exception.getMessage());
            }

        return result;
    }

    @Override
    public Result<ProductDTO> get(String uuid) throws BadRequestAlertException {
        Result<ProductDTO> result = new Result<>();
        result.setStatus(HttpStatus.OK.value());
        try {
            result.setData(this.productMapper.toDto(this.iProductTransactional.get(uuid).orElseThrow( () -> new Exception(String.format(Variables.MSG_NOT_EXIST_ENTITY,Variables.ENTITY_NAME_PRODUCT,uuid)))));
        }catch (Exception exception){
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setError(exception.getMessage());
        }
        return result;
    }

    @Override
    public Result<Boolean> delete(String uuid) throws BadRequestAlertException {
        Result<Boolean> result = new Result<>();
        try {
            this.iProductTransactional.delete(this.iProductTransactional.get(uuid).orElseThrow(() -> new BusinessException(String.format(Variables.MSG_NOT_EXIST_ENTITY,Variables.ENTITY_NAME_PRODUCT,uuid))));
            result.setStatus(HttpStatus.OK.value());
            result.setData(Boolean.TRUE);
        }catch (Exception exception){
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setError(exception.getMessage());
            result.setData(Boolean.FALSE);
        }
         return result;
    }

    @Override
    public Result< List<ProductDTO>> listAll() {
        Result< List<ProductDTO>> result = new Result<>();
        try {
            List<ProductDTO> list = this.productMapper.toDto(this.iProductTransactional.getAll());
            list.forEach(productDTO -> {
                productDTO.setPrice(productDTO.priceFinal());
            });
            result.setData(list);
            result.setStatus(HttpStatus.OK.value());
        }catch (Exception e){
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setError(e.getMessage());
            result.setData(Collections.emptyList());
        }
        return result;
    }

    @Override
    public Result<ProductDTO> productById(String uuid) throws BadRequestAlertException {
        Result<ProductDTO> result = new Result<>();

        try {
            result.setData(this.productMapper.toDto(this.iProductTransactional.get(uuid).orElseThrow(() -> new Exception(String.format(Variables.MSG_NOT_EXIST_ENTITY,Variables.ENTITY_NAME_PRODUCT,uuid)))));
            result.setStatus(HttpStatus.OK.value());
        }catch (Exception exception){
            result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setError(exception.getMessage());
        }
        return result;
    }
}
