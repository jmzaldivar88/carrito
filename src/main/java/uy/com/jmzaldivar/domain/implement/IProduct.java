package uy.com.jmzaldivar.domain.implement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Service
public interface IProduct {
    @JsonIgnoreProperties
    BigDecimal priceFinal();
}
