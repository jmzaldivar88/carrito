package uy.com.jmzaldivar.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uy.com.jmzaldivar.service.dto.ProductDTO;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.service.implement.IProductService;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.web.rest.implement.IProductResource;
import uy.com.jmzaldivar.web.rest.utils.ValidationUtil;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.data.util.Pair;

@RestController
@RequestMapping("/api/v1")
public class ProductResource implements IProductResource {



    @Autowired
    private IProductService iProductService;

    @Override
    public Result<ProductDTO> createProduct(ProductDTO product) {
        Result<ProductDTO> result = new Result<>();
        try {
            ValidationUtil.validParams(
                Pair.of("name", product.getName()),
                Pair.of("sku", product.getSku()),
                Pair.of("price",product.getPrice()),
                Pair.of("typeProduct",product.getTypeProduct())
            );
            result = this.iProductService.create(product);
        }catch (BusinessException exception){
            result.setError(exception.getMessage());
            result.setStatus(HttpStatus.BAD_REQUEST.value());
        }
        return result;
    }

    @Override
    public Result<ProductDTO> updateProduct(ProductDTO product) {
        Logger.getLogger(this.getClass().getName()).info(product.toString());
        Result<ProductDTO> result = new Result<>();
        try {
            ValidationUtil.validParams(
                Pair.of("idProduct", product.getIdProduct()),
                Pair.of("typeProduct",product.getTypeProduct())
            );
            result = this.iProductService.modify(product);
        }catch (BusinessException exception){
            result.setError(exception.getMessage());
            result.setStatus(HttpStatus.BAD_REQUEST.value());
        }
        return result;
    }

    @Override
    public Result<Boolean> deleteProduct(String uuid) {
        Result<Boolean> result = this.iProductService.delete(uuid);
        return result;
    }

    @Override
    public Result<List<ProductDTO>> listProduct() {
        Result<List<ProductDTO>>  result = this.iProductService.listAll();
        return result;
    }
}
