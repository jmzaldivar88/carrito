package uy.com.jmzaldivar.web.rest.implement;

import io.swagger.annotations.*;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uy.com.jmzaldivar.service.dto.TotalDto;
import uy.com.jmzaldivar.service.dto.ProductCartDTO;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.service.dto.ShoppingCartDTO;

import javax.validation.Valid;
import java.util.List;

@Validated
@Api(value = "product")
@Service
public interface IShoppingCartResouce {

    @ApiOperation(value = "Productos del carrito del usuario", nickname = "getAllProductByCarrito", notes = "", response = Result.class , tags={ "SHOPPING CART", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Response", response = Result.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/shopping_cart/product",
        produces = { "application/json" },
        method = RequestMethod.GET)
    Result<List<ProductCartDTO>> getAllProductByCarrito();

    @ApiOperation(value = "Verificar y confirmar compra", nickname = "checkout", notes = "", response = Result.class , tags={ "SHOPPING CART", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Response", response = Result.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/shopping_cart/checkout,",
        produces = { "application/json" },
        method = RequestMethod.GET)
    Result<TotalDto> checkout();

    @ApiOperation(value = "Adicionar producto al carrito", nickname = "addProductCart", notes = "", response = Result.class , tags={ "SHOPPING CART", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Response", response = Result.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/shopping_cart",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.POST)
    Result<Boolean> addProductCart(@ApiParam(value = "Carrito de compra" ,required=true )  @Valid @RequestBody ShoppingCartDTO cartDTO);


    @ApiOperation(value = "Modificar producto al carrito", nickname = "modifyProductCart", notes = "", response = Result.class , tags={ "SHOPPING CART", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Response", response = Result.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/shopping_cart",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    Result<Boolean> modifyProductCart(@ApiParam(value = "Carrito de compra" ,required=true )  @Valid @RequestBody ShoppingCartDTO cartDTO);


    @ApiOperation(value = "Eliminar producto del carrito", nickname = "deleteProduct", notes = "", response = Result.class, tags={ "SHOPPING CART", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Response", response = Result.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/shopping_cart/product/{uuidProduct}",
        produces = { "application/json" },
        method = RequestMethod.DELETE)
    Result<Boolean > deleteProduct(@ApiParam(value = "Identificador del producto" ,required=true )  @Valid @PathVariable(value = "uuidProduct",required = true) String uuidProduct);

}
