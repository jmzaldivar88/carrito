package uy.com.jmzaldivar.web.rest.implement;

import io.swagger.annotations.*;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uy.com.jmzaldivar.service.dto.ProductDTO;
import uy.com.jmzaldivar.web.rest.dto.Result;

import javax.validation.Valid;
import java.util.List;

@Validated
@Api(value = "product")
@Service
public interface IProductResource {


    @ApiOperation(value = "Crear producto", nickname = "createProduct", notes = "", response = Result.class , tags={ "PRODUCT", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Response", response = Result.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/product",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.POST)
    Result<ProductDTO > createProduct(@ApiParam(value = "Producto" ,required=true )  @Valid @RequestBody ProductDTO product);

    @ApiOperation(value = "Modificar producto", nickname = "updateProduct", notes = "", response = Result.class, tags={ "PRODUCT", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Response", response = Result.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/product",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    Result<ProductDTO> updateProduct(@ApiParam(value = "Producto" ,required=true )  @Valid @RequestBody ProductDTO product);


    @ApiOperation(value = "Eliminar producto", nickname = "deleteProduct", notes = "", response = Result.class, tags={ "PRODUCT", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Response", response = Result.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/product/{uuid}",
        produces = { "application/json" },
        method = RequestMethod.DELETE)
    Result<Boolean > deleteProduct(@ApiParam(value = "Identificador del producto" ,required=true )  @Valid @PathVariable("uuid") String uuid);


    @ApiOperation(value = "Listar todos los Producto", nickname = "listProduct", notes = "", response = Result.class, tags={ "PRODUCT", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Response", response = Result.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/product",
        produces = { "application/json" },
        method = RequestMethod.GET)
    Result<List<ProductDTO>> listProduct();

}
