package uy.com.jmzaldivar.web.rest.utils;

public class Variables {
    public static final String APP_NAME = "Carrito";
    public static final String ENTITY_NAME_PRODUCT = "Producto";
    public static final String ENTITY_NAME_SHOPPING_CART = "Carrito de compra";
    public  static final String MSG_NOT_EXIST_ENTITY = "No existe el %s con identificador %s";

}
