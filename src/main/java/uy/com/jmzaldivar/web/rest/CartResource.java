package uy.com.jmzaldivar.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uy.com.jmzaldivar.service.dto.TotalDto;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.service.implement.IShoppingCartService;
import uy.com.jmzaldivar.service.dto.ProductCartDTO;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.service.dto.ShoppingCartDTO;
import uy.com.jmzaldivar.web.rest.implement.IShoppingCartResouce;
import uy.com.jmzaldivar.web.rest.utils.ValidationUtil;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CartResource implements IShoppingCartResouce {

    @Autowired
    private IShoppingCartService iShoppingCartService;

    @Override
    public Result<List<ProductCartDTO>> getAllProductByCarrito() {
        return this.iShoppingCartService.listProductByCarrito(Boolean.FALSE);
    }

    @Override
    public Result<TotalDto> checkout() {
        return this.iShoppingCartService.checkout();
    }

    @Override
    public Result<Boolean> addProductCart(ShoppingCartDTO cartDTO){
        Result<Boolean> result = new Result<>();
        try {
            ValidationUtil.validParams(
                Pair.of("amount", cartDTO.getAmount()),
                Pair.of("product_id",cartDTO.getIdProducto())
            );
            result =  this.iShoppingCartService.create(cartDTO);
        } catch (BusinessException e) {
            result.setError(e.getMessage());
            result.setStatus(HttpStatus.BAD_REQUEST.value());
        }
        return result;
    }

    @Override
    public Result<Boolean> modifyProductCart(ShoppingCartDTO cartDTO) {
        Result<Boolean> result = new Result<>();
        try {
            ValidationUtil.validParams(
                Pair.of("amount", cartDTO.getAmount()),
                Pair.of("product_id",cartDTO.getIdProducto())
            );
            result =  this.iShoppingCartService.modify(cartDTO);
        } catch (BusinessException e) {
            result.setError(e.getMessage());
            result.setStatus(HttpStatus.BAD_REQUEST.value());
        }
        return result;
    }

    @Override
    public Result<Boolean> deleteProduct(String uuidProduct)  {
        return this.iShoppingCartService.delete(uuidProduct);
    }
}
