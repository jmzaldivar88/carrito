package uy.com.jmzaldivar.web.rest.utils;


import org.springframework.data.util.Pair;
import uy.com.jmzaldivar.service.error.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class ValidationUtil {

    public static void validParams(Pair<String, Object>... params) throws BusinessException {
        Map<String, Object> tmp = new HashMap<>();
        for (int i = 0; i < params.length; i++) {
            if (params[i].getSecond() == null || (params[i].getSecond() instanceof List && ((List) params[i].getSecond()).size() == 0)) {
                tmp.put("param" + i, params[i].getFirst());
            }
        }
        if (tmp.size() > 0) throw new BusinessException("Los campos no puedes ser null o vacios: "+tmp.values().stream().map(Object::toString).collect(Collectors.joining()));

    }
}
